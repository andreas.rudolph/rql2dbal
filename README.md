rql2dbal - Query SQL using RQL and Doctrine DBAL in PHP
==============

This is a wrapper around [graviton/rql-parser](https://github.com/libgraviton/rql-parser) that combines that parser with a small layer of Doctrine DBAL integration.

## Installation

Install it using [composer](https://getcomposer.org/).

```bash
composer require chronext/rql2dbal
```

## Usage

```php
<?php declare(strict_types=1);

require 'vendor/autoload.php';

use Chronext\Rql2Dbal\Rql2Builder;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Statement;

class Finder
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var Rql2Builder
     */
    private $rql2Builder;

    public function __construct(Connection $connection, Rql2Builder $rql2Builder)
    {
        $this->connection  = $connection;
        $this->rql2Builder = $rql2Builder;
    }

    /**
     * @return \Generator
     */
    public function query(string $query): iterable
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->from('table_name');

        ($this->rql2Builder)(
            $qb,
            $query,
            [
                'uuid',
                'field1',
                'field2',
            ]
        );

        $stmt = $qb->execute();
        if (!($stmt instanceof Statement)) {
            throw new \LogicException('Query builder has to return only Statement instance in this case.');
        }

        while ($row = $stmt->fetch()) {
            yield $row;
        }
    }
}

$query = 'or(eq(field1,foo)&eq(field2,bar))';
$orders = (new Finder(new Connection($params,$driver), new Rql2Builder()))->query($query);
```
