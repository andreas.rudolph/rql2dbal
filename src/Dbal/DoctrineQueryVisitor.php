<?php declare(strict_types=1);

namespace Chronext\Rql2Dbal\Dbal;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Types\Types;
use Graviton\RqlParser\Glob;
use Graviton\RqlParser\Node\AbstractQueryNode;
use Graviton\RqlParser\Node\LimitNode;
use Graviton\RqlParser\Node\Query\AbstractArrayOperatorNode;
use Graviton\RqlParser\Node\Query\AbstractLogicalOperatorNode;
use Graviton\RqlParser\Node\Query\AbstractScalarOperatorNode;
use Graviton\RqlParser\Node\Query\LogicalOperator\AndNode;
use Graviton\RqlParser\Node\SelectNode;
use Graviton\RqlParser\Node\SortNode;
use Graviton\RqlParser\Query;

class DoctrineQueryVisitor
{
    /**
     * @var string[]
     */
    private $availableFields;
    /**
     * @var string[]
     */
    private $alwaysFields;

    /**
     * @param string[] $availableFields
     * @param string[] $alwaysFields
     */
    private function __construct(array $availableFields, array $alwaysFields)
    {
        $this->availableFields = $availableFields;
        $this->alwaysFields = $alwaysFields;
    }

    /**
     * @param string[] $availableFields
     * @param string[] $alwaysFields
     */
    public static function withAvailableFields(array $availableFields, array $alwaysFields = []): self
    {
        return new self($availableFields, $alwaysFields);
    }

    public function visit(Query $query, QueryBuilder $doctrineQueryBuilder): void
    {
        $this->visitSelectNode($query->getSelect(), $doctrineQueryBuilder);
        /** @psalm-suppress RedundantConditionGivenDocblockType it actually can return null */
        if ($query->getQuery() !== null) {
            $this->visitQueryNode($query->getQuery(), $doctrineQueryBuilder);
        }
        /** @psalm-suppress RedundantConditionGivenDocblockType it actually can return null */
        if ($query->getSort() !== null) {
            $this->visitSortNode($query->getSort(), $doctrineQueryBuilder);
        }
        /** @psalm-suppress RedundantConditionGivenDocblockType it actually can return null */
        if ($query->getLimit() !== null) {
            $this->visitLimitNode($query->getLimit(), $doctrineQueryBuilder);
        }
    }

    private function visitQueryNode(AbstractQueryNode $node, QueryBuilder $qb): void
    {
        if (!($node instanceof AbstractLogicalOperatorNode)) {
            $node = new AndNode([$node]);
        }
        $this->visitLogicalNode($node, $qb);
    }

    private function getScalarExpression(AbstractQueryNode $node, QueryBuilder $qb): string
    {
        if ($node instanceof AbstractScalarOperatorNode) {
            return $this->getScalarNode($node, $qb);
        }
        if ($node instanceof AbstractArrayOperatorNode) {
            return $this->getArrayNode($node, $qb);
        }

        throw new \LogicException(sprintf('Unknown node "%s" in method "%s"', $node->getNodeName(), __METHOD__));
    }

    private function visitSelectNode(?SelectNode $node, QueryBuilder $qb): void
    {
        $fields = $this->availableFields;
        if (null !== $node) {
            $fields = array_intersect($fields, $node->getFields());
            if (count($fields) < $node->getFields()) {
                throw new \UnexpectedValueException(
                    sprintf(
                        'You can use only "%s" fields. max(), min(), avg(), etc. did not implemented yet.',
                        implode(' ,', $this->availableFields)
                    )
                );
            }
        }
        $fields = array_merge($fields, $this->alwaysFields);
        $qb->select($fields);
    }

    private function visitSortNode(SortNode $node, QueryBuilder $qb): void
    {
        /** @var array<string, int> $fields */
        $fields = $node->getFields();
        $qb->orderBy(
            implode(
                ', ',
                array_map(
                    function (string $field, int $direction): string {
                        return $this->encodeField($field).' '.($direction > 0 ? 'ASC' : 'DESC');
                    },
                    array_keys($fields),
                    array_values($fields)
                )
            )
        );
    }

    private function visitLimitNode(LimitNode $node, QueryBuilder $qb): void
    {
        $qb->setFirstResult($node->getOffset());
        $qb->setMaxResults((int)$node->getLimit());
    }

    private function getScalarNode(AbstractScalarOperatorNode $node, QueryBuilder $qb): string
    {
        /** @var array<string,string> $operators */
        $operators = [
            'like' => 'like',
            'eq'   => '=',
            'ne'   => '<>',
            'lt'   => '<',
            'gt'   => '>',
            'le'   => '<=',
            'ge'   => '>=',
        ];
        if (!isset($operators[$node->getNodeName()])) {
            throw new \UnexpectedValueException(sprintf('Unknown scalar node "%s"', $node->getNodeName()));
        }
        $this->checkField($node->getField());
        /** @var int|string|Glob|\DateTimeInterface $value */
        $value    = $node->getValue();
        $bindType = null;
        if ($value instanceof Glob && $node->getNodeName() === 'like') {
            $value = $value->toLike();

            return $qb->expr()->like($node->getField(), $value);
        }
        if ($value instanceof \DateTime) {
            $bindType = Types::DATETIMETZ_MUTABLE;
        }
        if ($value instanceof \DateTimeImmutable) {
            $bindType = Types::DATETIMETZ_IMMUTABLE;
        }

        $bindName = uniqid($node->getField(), false);
        $qb->setParameter($bindName, $value, $bindType);

        return $qb->expr()->comparison($node->getField(), $operators[$node->getNodeName()], ":$bindName");
    }

    private function getArrayNode(AbstractArrayOperatorNode $node, QueryBuilder $qb): string
    {
        $this->checkField($node->getField());

        switch ($node->getNodeName()) {
            case ('in'):
                {
                    /**
                     * @psalm-suppress MixedTypeCoercion We can work only with array of strings,
                     *                                    but can not guaranty it.
                     *                                   We should fix it.
                     */
                    return $qb->expr()->in($node->getField(), $node->getValues());
                }
            case ('out'):
                {
                    /**
                     * @psalm-suppress MixedTypeCoercion We can work only with array of strings,
                     *                                    but can not guaranty it.
                     *                                   We should fix it.
                     */
                    return $qb->expr()->notIn($node->getField(), $node->getValues());
                }
            default:
                {
                    throw new \UnexpectedValueException(sprintf('Unknown array node "%s"', $node->getNodeName()));
                }
        }
    }

    private function visitLogicalNode(AbstractLogicalOperatorNode $node, QueryBuilder $qb): void
    {
        $clauses = [];
        foreach ($node->getQueries() as $index => $query) {
            $clauses[] = $this->getScalarExpression($query, $qb);
        }

        switch ($node->getNodeName()) {
            case ('not'):
                {
                    throw new \UnexpectedValueException(
                        sprintf('Logical node "%s" did not implemented.', $node->getNodeName())
                    );
                }
            case ('and'):
                {
                    $qb->andWhere($qb->expr()->andX(...$clauses));
                    break;
                }
            case ('or'):
                {
                    $qb->andWhere($qb->expr()->orX(...$clauses));
                    break;
                }
            default:
                {
                    throw new \UnexpectedValueException(sprintf('Unknown logical node "%s"', $node->getNodeName()));
                }
        }
    }

    private function encodeField(string $field): string
    {
        $this->checkField($field);

        return '`'.$field.'`';
    }

    private function checkField(string $field): void
    {
        if (!in_array($field, $this->availableFields, true)) {
            $availableFields = implode(' ,', $this->availableFields);
            throw new \InvalidArgumentException("The field '${field}' is not available in the '$availableFields'");
        }
    }
}
