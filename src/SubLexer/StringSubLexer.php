<?php declare(strict_types=1);

namespace Chronext\Rql2Dbal\SubLexer;

use Graviton\RqlParser\SubLexerInterface;
use Graviton\RqlParser\Token;

class StringSubLexer implements SubLexerInterface
{
    /**
     * @inheritdoc
     */
    public function getTokenAt($code, $cursor): ?Token
    {
        if (!preg_match('/([a-z0-9_]|\%[0-9a-f]{2})+/Ai', $code, $matches, 0, $cursor)) {
            return null;
        }
        if (ctype_digit($matches[0])) {
            return null;
        }

        return new Token(
            Token::T_STRING,
            rawurldecode($matches[0]),
            $cursor,
            $cursor + strlen($matches[0])
        );
    }
}
