<?php declare(strict_types=1);

namespace Chronext\Rql2Dbal;

use Chronext\Rql2Dbal\Dbal\DoctrineQueryVisitor;
use Doctrine\DBAL\Query\QueryBuilder;
use Graviton\RqlParser\Lexer;
use Graviton\RqlParser\Parser;

class Rql2Builder
{
    /**
     * @var Parser
     */
    private $parser;
    /**
     * @var Lexer
     */
    private $lexer;

    public function __construct(?Lexer $lexer = null, ?Parser $parser = null)
    {
        $this->lexer  = $lexer ?? new Lexer();
        $this->parser = $parser ?? new Parser();
    }

    /**
     * @param string[] $availableFields
     * @param string[] $alwaysFields
     */
    public function __invoke(QueryBuilder $qb, string $queryString, array $availableFields, array $alwaysFields = []): void
    {
        $token       = $this->lexer->tokenize($queryString);
        $query       = $this->parser->parse($token);
        $nodeVisitor = DoctrineQueryVisitor::withAvailableFields($availableFields, $alwaysFields);

        $nodeVisitor->visit($query, $qb);
    }
}
